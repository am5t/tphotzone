from protorpc import messages


class Attribute(messages.Message):
    name = messages.StringField(1)
    value = messages.StringField(2)


class ItemRequest(messages.Message):
    name = messages.StringField(3, required=True)
    location = messages.StringField(4, required=True)
    attributes = messages.MessageField(Attribute, 5, repeated=True)


class ItemResponse(messages.Message):
    id = messages.StringField(1, required=True)
    name = messages.StringField(2, required=True)
    location = messages.StringField(3, required=True)
    attributes = messages.MessageField(Attribute, 4, repeated=True)
    broker_system = messages.StringField(5, required=True)


class ItemCollection(messages.Message):
    items = messages.MessageField(ItemResponse, 1, repeated=True)
