import logging
import cPickle as pickle


def set_memcache(memcache, bs_id, items):
    logging.debug('set_memcache')
    chunk_size = memcache.MAX_VALUE_SIZE
    pickle_string = pickle.dumps(items)
    chunks = split_into_chunks(pickle_string, chunk_size)
    for chunk_num, chunk in enumerate(chunks):
        key = '%s_%s' % (str(bs_id), chunk_num)
        memcache.set(key, chunk)


def get_memcache(memcache, bs_id):
    chunks = []
    while True:
        key = '%s_%s' % (str(bs_id), str(len(chunks)))
        chunk = memcache.get(key)
        if chunk is None:
            break
        chunks.append(chunk)
    try:
        items_pickle = merge_chunks(chunks)
        logging.debug('cache hit')
        return pickle.loads(items_pickle)
    except:
        return None


def split_into_chunks(msg, chunk_size):
    chunks = []
    while msg:
        chunk = msg[:chunk_size]
        msg = msg[chunk_size:]
        chunks.append(chunk)
    return chunks


def merge_chunks(chunks):
    msg = ''.join(chunks)
    return msg
