import logging

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote
from google.appengine.ext import ndb
from google.appengine.api import memcache

from geopy.distance import vincenty
import json

from .message import (ItemRequest, ItemResponse, ItemCollection)
from .model import (Item)
import utils
from .memcached import set_memcache, get_memcache

TIME_FMT = "%d. %B %Y %I:%M%p"


@utils.esbs.api_class(resource_name='item')
class ItemApi(remote.Service):
    LIST_RESOURCE = endpoints.ResourceContainer(
            message_types.VoidMessage,
            bs_id=messages.IntegerField(1, required=True))

    @endpoints.method(LIST_RESOURCE, ItemCollection,
                      path='items/{bs_id}', http_method='GET',
                      name='list')
    def list(self, request):
        bs_key = ndb.Key("BrokerSystem", request.bs_id)
        return self._list_items(bs_key)

    SEARCH_RESOURCE = endpoints.ResourceContainer(
            message_types.VoidMessage,
            bs_id=messages.IntegerField(1, required=True),
            location=messages.StringField(2, required=True),
            distance=messages.IntegerField(3, required=True),
            service_type=messages.StringField(4))

    @endpoints.method(SEARCH_RESOURCE, ItemCollection,
                      path='items/{bs_id}/{location}', http_method='GET',
                      name='search')
    def search(self, request):
        bs_key = ndb.Key("BrokerSystem", request.bs_id)
        return self._list_items(bs_key)

    def _list_items(self, bs_key):
        bs = bs_key.get()
        if bs:
            cached_items = get_memcache(memcache, str(bs_key.id()))
            if cached_items:
                return ItemCollection(items=cached_items)
            logging.debug(bs.filepath)
            with open(bs.filepath) as fp:
                entities = json.loads(fp.read())
                items = [self.build_response(bs, e) for e in entities if e.get('location')]
                set_memcache(memcache, bs_key.id(), items)
                return ItemCollection(items=items)
        else:
            raise endpoints.NotFoundException('notFound')

    @classmethod
    def build_response(cls, broker_system, entity):
        return ItemResponse(id=entity['id'], name=entity['name'],
                attributes=entity['attributes'], location=entity['location'],
                broker_system=str(broker_system))
