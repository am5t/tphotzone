import endpoints


MANAGERS = ['taipeihotzone@gmail.com']
WEB_CLIENT_ID = '690150048812-hsefnmrj6sjhin7g8f6ri6ti7hgpahvt.apps.googleusercontent.com'
ALLOWED_CLIENT_IDS = [WEB_CLIENT_ID,
                      endpoints.API_EXPLORER_CLIENT_ID]


def admin_only(func):
    def outerfunc(*args, **kwargs):
        if endpoints.get_current_user().email() not in MANAGERS:
            raise endpoints.ForbiddenException('manager protected function')
        return func(*args, **kwargs)
    return outerfunc


def login_only(func):
    def outerfunc(*args, **kwargs):
        if not endpoints.get_current_user():
            raise endpoints.UnauthorizedException('Please login')
        return func(*args, **kwargs)
    return outerfunc


esbs = endpoints.api(name='esbs', version='v1.0',
                     description='ESBS API v1',
                     allowed_client_ids=ALLOWED_CLIENT_IDS)
