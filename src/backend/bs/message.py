from protorpc import messages


class BrokerSystemRequest(messages.Message):
    name = messages.StringField(2, required=True)
    filepath = messages.StringField(3, required=True)


class BrokerSystemResponse(messages.Message):
    id = messages.IntegerField(1, variant=messages.Variant.INT32,
                               required=True)
    name = messages.StringField(2, required=True)
    filepath = messages.StringField(3, required=True)


class BrokerSystemCollection(messages.Message):
    broker_systems = messages.MessageField(BrokerSystemResponse, 1,
                                           repeated=True)
