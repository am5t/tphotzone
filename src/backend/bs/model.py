from google.appengine.ext import ndb


class BrokerSystem(ndb.Model):
    name = ndb.StringProperty()
    filepath = ndb.StringProperty()
