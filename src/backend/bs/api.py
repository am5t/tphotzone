import logging

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote
from google.appengine.ext import ndb

from .message import (BrokerSystemRequest, BrokerSystemResponse,
                   BrokerSystemCollection)
from .model import (BrokerSystem)
import utils


@utils.esbs.api_class(resource_name='bs')
class BrokerSystemApi(remote.Service):
    @endpoints.method(BrokerSystemRequest, BrokerSystemResponse,
                      path='bs', http_method='PUT', name='create')
    @utils.login_only
    @utils.admin_only
    def create(self, request):
        logging.debug('create broker_system %s' % request.name)
        bs = BrokerSystem(name=request.name, filepath=request.filepath)
        key = bs.put()
        return self.build_response(key)

    UPDATE_RESOURCE = endpoints.ResourceContainer(
            BrokerSystemRequest,
            id=messages.IntegerField(1,
                                     variant=messages.Variant.INT64,
                                     required=True))

    @endpoints.method(UPDATE_RESOURCE, BrokerSystemResponse,
                      path='bs/{id}', http_method='PUT',
                      name='update')
    @utils.login_only
    @utils.admin_only
    def update(self, request):
        key = ndb.Key("BrokerSystem", request.id)
        bs = BrokerSystem(key=key, name=request.name, filepath=request.filepath)
        bs.put()
        return self.build_response(key)

    RETRIEVE_RESOURCE = endpoints.ResourceContainer(
            message_types.VoidMessage,
            id=messages.IntegerField(1,
                                     variant=messages.Variant.INT64))

    @endpoints.method(RETRIEVE_RESOURCE, BrokerSystemResponse,
                      path='bs/{id}', http_method='GET',
                      name='retrieve')
    @utils.login_only
    def retrieve(self, request):
        key = ndb.Key("BrokerSystem", request.id)
        return self.build_response(key)

    @endpoints.method(message_types.VoidMessage, BrokerSystemCollection,
                      path='bss', http_method='GET', name='list')
    @utils.login_only
    @utils.admin_only
    def list(self, request):
        qry = BrokerSystem.query()
        bss = [self.build_response(k) for k in qry.iter(keys_only=True)]
        return BrokerSystemCollection(broker_systems=bss)

    @classmethod
    def build_response(cls, key):
        bs = key.get()
        if bs:
            return BrokerSystemResponse(id=key.id(), name=bs.name,
                                        filepath=bs.filepath)
        else:
            raise endpoints.NotFoundException('notFound')
