import logging
import requests
import argparse
import json

from hotzone_client import HotzoneClient
from oauth2 import OAuth2Crendentials

SITENAMES = [
#             'tphotzone',
            'twn-h1',
            'twn-h2',
            'twn-h3',
            'twn-h4',
            'twn-h5',
            'twn-h6',
            'twn-h7',
            'twn-h8',
            ]

if __name__ == '__main__':
    logging.getLogger("requests").setLevel(logging.WARNING)
    requests.packages.urllib3.disable_warnings()
    parser = argparse.ArgumentParser(description='bs_maker')
    parser.add_argument('-a', '--auth_prefix', metavar='<auth_prefix>',
                        type=str, required=True,
                        help=('a prefix of file in json contains tokens'))
    parser.add_argument('-f', '--filepath', metavar='<filepath>',
                        type=str, required=True,
                        help=('filepath of source json'))
    parser.add_argument('name', type=str, help=('name of this bs'))
    config = {
        'connection': {'max_retry_num': 7}
    }
    args = parser.parse_args()
    for sitename in SITENAMES:
        site = "https://{0}.appspot.com/".format(sitename)
        auth_file = args.auth_prefix + sitename
        with open(auth_file) as fp:
            auth_config = json.loads(fp.read())
        cred = OAuth2Crendentials(auth_config)
        client = HotzoneClient(site=site, credentials=cred, config=config)
        print site
        bs = client.create_bs(args.name, args.filepath)
        print bs['id']
