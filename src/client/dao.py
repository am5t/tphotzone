#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json


class JsonFileDAO(object):
    def __init__(self, json_file):
        with open(json_file) as fp:
            items = json.loads(fp.read())
            self.iter = iter(items)

    def next(self):
        return self.iter.next()


if __name__ == '__main__':
    dao = JsonFileDAO('/tmp/test.json')
    while True:
        try:
            print dao.next()
        except StopIteration:
            break
