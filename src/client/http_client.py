import logging
import os
import random
import time

from requests.exceptions import ConnectionError, ProxyError, SSLError, Timeout
from requests import Request

LOG = logging.getLogger(__name__)


class BearHeaderMixin(object):
    def get_access_token(self):
        pass

    def add_headers(self, req):
        req.headers['Authorization'] = 'Bearer ' + self.get_access_token()


class RefreshTokenMixin(object):
    def refresh_token(self):
        pass

    def process_resp(self, resp):
        expect_code = self._config.get('status_code_need_refresh_token', [401])
        if resp.status_code in expect_code:
            LOG.info('refresh_token')
            self.refresh_token()


class ExponentialBackoffMixin(object):
    def backoff(self, retry_count):
        delay = random.random() * (2 ^ min(8, retry_count))
        time.sleep(delay)


def retry(func):
    def outter_func(self, *args, **kwargs):
        conn_config = self._config['connection']
        max_retry = conn_config.get('max_retry_num', 10)
        retry_count = 0
        while True:
            resp = None
            error = None
            try:
                resp = func(self, *args, **kwargs)
            except (ConnectionError, ProxyError, SSLError, Timeout) as ex:
                # For these exceptions, we should retry.
                error = ex
            if retry_count >= max_retry:
                break
            # resp will return 0 if defined __nonezero__, and resp
            # (instance of requests.models.Response) define this func to return
            # the status code.
            # https://docs.python.org/2/library/stdtypes.html
            if not resp or self.is_temp_failure(resp) is False:
                break
            elif hasattr(self, 'backoff'):
                self.backoff(retry_count)
            retry_count += 1
        if error:
            raise error
        return resp
    return outter_func


def handle_token_expire(func):
    def outter_func(self, *args, **kwargs):
        need_refresh = self._config.get('status_code_need_refresh_token',
                                        [401])
        conn_config = self._config['connection']
        max_retry = conn_config.get('max_refresh_token_retry_num', 5)
        # backoff = conn_config.get('backoff', False)
        retry_count = 0
        while True:
            resp = None
            error = None
            try:
                resp = func(self, *args, **kwargs)
            except Exception as ex:
                error = ex
            if retry_count >= max_retry:
                break
            if resp is not None and resp.status_code not in need_refresh:
                break
            if hasattr(self, 'backoff'):
                self.backoff(retry_count)
            retry_count += 1
        if error:
            raise error
        return resp
    return outter_func


class HttpClient(object):
    '''Initializer

    self._config: HttpClient configuration, a dict containing
        - connection
            - max_retry_num (int): specify maximum number of retry. Defaut 0.
            - status_code_need_retry (list): specify http status
              codes need retry. Default [500, 503]
            - timeount (tuple): specify the connect and the read
              timeout. Default (60, 60).
    '''
    def _concate_url(self, url):
        if isinstance(url, tuple):
            relpath = os.path.normpath('/'.join(url[1:]))
            if relpath[0] == '/':
                relpath = relpath[1:]
            url = os.path.join(url[0], relpath)
        return url

    @retry
    @handle_token_expire
    def _do_http_request(self, method='GET', url='', stream=False,
                         data=None, params=None, files=None, headers=None):
        url = self._concate_url(url)
        req = Request(method, url, data=data, params=params,
                      headers=headers)
        for cls in self.__class__.__bases__:
            if hasattr(cls, 'add_headers'):
                cls.add_headers(self, req)
        preq = req.prepare()
        sess = self._session
        timeout = self._config['connection'].get('timeout', (60, 60))
        resp = sess.send(preq, stream=stream, verify=False, timeout=timeout)
        for cls in self.__class__.__bases__:
            if hasattr(cls, 'process_resp'):
                cls.process_resp(self, resp)
        return resp
