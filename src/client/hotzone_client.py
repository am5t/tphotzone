import logging
from requests import Session
import json

from http_client import HttpClient, RefreshTokenMixin, BearHeaderMixin


LOG = logging.getLogger(__name__)


class HotzoneClient(HttpClient, BearHeaderMixin, RefreshTokenMixin):
    def __init__(self, *args, **kwargs):
        self._credentials = kwargs.get('credentials')
        self.site = kwargs.get('site')
        self._config = kwargs.get('config')
        self._session = Session()

    def get_access_token(self):
        return self._credentials.access_token

    def refresh_token(self):
        self._credentials.refresh()

    def is_temp_failure(self, resp):
        return resp.status_code in [500, 503]

    def get_bs_id(self, bs_name):
        resp = self._do_http_request(url=(self.site, '/_ah/api/esbs/v1.0/bss'))
        resp.raise_for_status()
        boards = resp.json()['broker_systems']
        for board in boards:
            if board['name'] == bs_name:
                return board['id']
        raise RuntimeError('No such bs_name %s' % bs_name)

    def create_bs(self, bs_name, filepath):
        headers = {'content-type': 'application/json'}
        data = {"name": bs_name, "filepath": filepath}
        resp = self._do_http_request(method='PUT',
                                     url=(self.site,
                                          '/_ah/api/esbs/v1.0/bs'),
                                     data=json.dumps(data),
                                     headers=headers)
        resp.raise_for_status()
        return resp.json()

    def create_item(self, bs_id, item):
        headers = {'content-type': 'application/json'}
        resp = self._do_http_request(method='PUT',
                                     url=(self.site,
                                          '/_ah/api/esbs/v1.0/item/%s' %
                                          bs_id),
                                     data=json.dumps(item),
                                     headers=headers)
        resp.raise_for_status()
