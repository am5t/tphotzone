import argparse
import requests
import json

URL = "https://www.googleapis.com/oauth2/v3/token"

if __name__ == '__main__':
    requests.packages.urllib3.disable_warnings()
    parser = argparse.ArgumentParser(description='get token')
    parser.add_argument('-s', '--client_secret', metavar='<client_secret>',
                        type=str, required=True, help=('client_secret'))
    parser.add_argument('-r', '--redirect_uri', metavar='<redirect_uri>',
                        default="http://localhost:9004",
                        type=str, required=False, help=('redirect_uri'))
    parser.add_argument('-c', '--client_id', metavar='<client_id>',
                        type=str, required=True, help=('client_id'))
    parser.add_argument('-k', '--code', metavar='<code>',
                        type=str, required=True, help=('code'))
    parser.add_argument('-g', '--grant_type', metavar='<grant_type>',
                        default="authorization_code",
                        type=str, required=False, help=('grant_type'))
    args = parser.parse_args()
    params = {
              'grant_type': args.grant_type,
              'redirect_uri': args.redirect_uri,
              'code': args.code,
              'client_id': args.client_id,
              'client_secret': args.client_secret
              }
    resp = requests.post(URL, params=params)
    result = resp.json()
    for key in ['id_token', 'expires_in', 'token_type']:
        if key in result:
            del result[key]
    result['refresh_url'] = 'https://www.googleapis.com/oauth2/v3/token'
    result['client_secret'] = args.client_secret
    result['client_id'] = args.client_id
    print json.dumps(result, indent=2)

