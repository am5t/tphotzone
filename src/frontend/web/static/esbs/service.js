var item_service = angular.module('ItemService', []);
item_service.factory('backend', function($q) {
    this.search_items = function(bs_id, location, distance) {
        return $q(function(resolve, reject) {
            gapi.client.esbs.item.search({
                'bs_id': bs_id,
                'location': location,
                'distance': distance
            }).execute(
                function(resp) {
                    if (!resp.code) {
                        resolve(resp.items);
                    }
                }
            );
        });
    };
    return {
        search_items: this.search_items
    };
});

