(function(angular) {
    'use strict';
    var esbs_module = angular.module('ESBS', ['uiGmapgoogle-maps', 'ItemService', 'cgBusy']);

    esbs_module.controller('MapController', ['backend', '$scope', '$log', '$http',
                                       function(backend, $scope, $log, $http) {
        var parent = this;
        var address;
        var distance = 10;
        $scope.bs_id = null;

        /* map controller */

        var clear = function clear(){
            parent.items = [];
            parent.target_item = null;
        };

        var coords2str = function coords2str(coords){
            var location = coords.latitude + ","  + coords.longitude;
            return location
        };

        var str2coords = function str2coords(str){
            var parts = str.split(',');
            var coords = {
                latitude: parts[0],
                longitude: parts[1]
            }
            return coords
        };
        var search_items = function search_items(bs_id, center, distance) {
            var location = coords2str(center);
            $scope.search_promise = backend.search_items(bs_id, location, distance);
            $scope.search_promise.then(function(promise) {
                if (promise){
                    parent.items = promise;
                    var i;
                    for (i = 0; i < parent.items.length; i++) {
                        parent.items[i].coords = str2coords(parent.items[i].location);
                    }
                }else{
                    parent.items = [];
                }
            });
        };

        var set_category = function set_category(category) {
            $scope.category = category;
            $scope.bs_id = null;
            clear();
        };

        var set_broker_system = function set_broker_system(bs_name) {
            $scope.bs_id = BROKER_SYSTEM_MAPPING[bs_name];
            clear();
            search_items($scope.bs_id, $scope.map.center, distance);
        };

        var dragend_handler = function () {
        };

        var search_address = function query_address(address) {
            var query = 'https://maps.google.com/maps/api/geocode/json?address=' + address + '=&sensor=false'
            $http.get(query).success(function(resp) {
                var target = resp.results.pop()
                if (target) {
                    set_center(target.geometry.location.lat, target.geometry.location.lng);
                }
            });
        };

        var set_center = function set_center(latitude, longitude) {
            var center = {latitude: latitude, longitude: longitude};
            $scope.map.control.refresh(center);
            $scope.map.center = center;
            search_items($scope.bs_id, $scope.map.center, distance);
        };

        var sel_item = function sel_item(item) {
            parent.target_item = item;
        };

        /* map */
        $scope.map = {
            center: {latitude: 25.0478, longitude: 121.5172},
            zoom: 16,
            control: {},
            options: {scrollwheel: false},
            events: {
                dragend: dragend_handler
            },
            info_window: {
                options: {
                    boxClass: 'info-window red-info-window',
                    closeBoxDiv: '<div" class="pull-right" style="position: relative; cursor: pointer; margin: -20px -15px;">X</div>',
                    closeBoxURL: '/resources/close.gif',
                    disableAutoPan: true
                }
            }
        };

        $scope.$root.sel_item = sel_item;
        this.set_center = set_center;
        this.search_items = search_items;
        this.search_address = search_address;
        this.set_category = set_category;
        this.set_broker_system = set_broker_system;
    }]);
})(window.angular);
