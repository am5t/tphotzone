import sys
import json
import io
import argparse


class JsonFileDAO(object):
    def __init__(self, json_file):
        with open(json_file) as fp:
            items = json.loads(fp.read())
            self.iter = iter(items)

    def next(self):
        return self.iter.next()


def commandline_arg(bytestring):
    unicode_string = bytestring.decode(sys.getfilesystemencoding())
    return unicode_string


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='tphotzone client')
    parser.add_argument('-a', '--attribute', metavar='<attribute>',
                        type=commandline_arg, required=True,
                        help=('attribute name to be added'))
    parser.add_argument('-v', '--value', metavar='<value>',
                        type=commandline_arg, required=True,
                        help=('attribute value to be added'))
    parser.add_argument("source", type=str, help=('source file'))
    args = parser.parse_args()
    input_file = args.source
    output_file = input_file

    dao = JsonFileDAO(input_file)

    items = []
    while True:
        try:
            item = dao.next()
            attribute = {}
            attribute[u'name'] = args.attribute
            attribute[u'value'] = args.value
            item[u'attributes'].append(attribute)
            items.append(item)
        except StopIteration:
            break

    with io.open(output_file, 'w', encoding='utf8') as fp:
        json_str = json.dumps(items, ensure_ascii=False, indent=2)
        fp.write(json_str)
